import fs from 'fs'

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(new Date (currentDate));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

const dateRange = getDates(new Date('2023-10-29'), new Date('2023-12-31'))

dateRange.forEach( async(date) => {
    // insert to scripts
    const localeDateString = date.toLocaleDateString().split('/');
    const calendar = `${localeDateString[2]}-${localeDateString[0]}-${localeDateString[1].length == 1 ? '0' + localeDateString[1] : localeDateString[1]}` 
    const script = 
    `
    INSERT INTO carsome_rds_v3.marketplaces(\`type\`, \`live_date\`, \`end_date\`, \`total_cars_bid\`, \`slot\`, \`total_bids\`, \`total_dealers_bid\`, \`total_dealers_view\`, \`total_dealers_active\`, \`added_by\`, \`created_at\`, \`updated_at\`, \`editor_type\`, \`used_car_marketplace_1_id\`, \`extended_time\`)
    SELECT 'buyNow', '${calendar} 12:00:00' , '${calendar} 13:00:00', 0, 200, 0, 0, 0, 0, 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 0, NULL FROM DUAL
    WHERE NOT EXISTS
    (
        SELECT * from \`marketplaces\` AS m
        WHERE (m.live_date >= @live_date AND m.live_date <= @end_date AND type != 'open')
        OR (m.end_date <= @end_date AND m.end_date >= @live_date AND type != 'open')
        OR (m.live_date <= @live_date AND m.end_date >= @end_date AND type != 'open')
        AND (type != 'jit')
    ); 

    INSERT INTO carsome_rds_v3.marketplaces(\`type\`, \`live_date\`, \`end_date\`, \`total_cars_bid\`, \`slot\`, \`total_bids\`, \`total_dealers_bid\`, \`total_dealers_view\`, \`total_dealers_active\`, \`added_by\`, \`created_at\`, \`updated_at\`, \`editor_type\`, \`used_car_marketplace_1_id\`, \`extended_time\`)
    SELECT 'buyNow', '${calendar} 15:00:00' , '${calendar} 16:00:00', 0, 200, 0, 0, 0, 0, 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 0, NULL FROM DUAL
    WHERE NOT EXISTS
    (
        SELECT * from \`marketplaces\` AS m
        WHERE (m.live_date >= @live_date AND m.live_date <= @end_date AND type != 'open')
        OR (m.end_date <= @end_date AND m.end_date >= @live_date AND type != 'open')
        OR (m.live_date <= @live_date AND m.end_date >= @end_date AND type != 'open')
        AND (type != 'jit')
    ); 
    `
    const current = new Date();
    await fs.writeFile(`./scripts/sqlQueries-${current.toISOString().split('T')[0]}.script.txt`, script,
        {
            encoding: "utf8",
            flag: "a",
            mode: 0o666
        },
        (err) => {
            if (err)
            console.log(err);
        });
})
