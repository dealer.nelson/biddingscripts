const jwt = require('jsonwebtoken')
require('dotenv').config()

const payload = {
  userId: 999,
  type: 'service',
};

const token = jwt.sign(payload, process.env.TOKEN_SECRET, {
  expiresIn: '600 minutes'
});

console.log(token)  