import rdsData from './rds.json' assert { type: "json"};
import mongoData from './mongo.json' assert { type: "json"};
import fs from 'fs'

// prepare mongo data 
const carIdList = {}
mongoData.forEach( (data) => {
    carIdList[data.inspectionId] = data._id.$oid
})

const current = new Date();
// cross check data and generate to a file or log out
rdsData.forEach( (data) => {
    if (data.inspection_id in carIdList) {
        // generate data 
        const script = `
    db.cars.updateOne(
        { _id: ObjectId('${carIdList[data.inspection_id]}') },
        {
            $set: {
            dealerAssigned: {
                usedDealerCompanyId: '${data.assigned_to}',
                dealerId: '${data.dealer_id}',
                amount: ${data.assigned_price},
                assignedAt: ISODate('${current.toISOString()}'),
                assignerId: '9',
                assignerType: 'service'
            },
            dealerAssignLogs: [
                {
                usedDealerCompanyId: '${data.assigned_to}',
                dealerId: '${data.dealer_id}',
                amount: ${data.assigned_price},
                assignedAt: ISODate('${current.toISOString()}'),
                assignerId: '9',
                assignerType: 'service'
                }
            ]
            }
        }
        );
          `
        // write all to a script file
        fs.writeFile(`./scripts/syncAutoAssignCars-${current.toISOString().split('T')[0]}.script.txt`, script,
        {
            encoding: "utf8",
            flag: "a",
            mode: 0o666
        },
        (err) => {
            if (err)
            console.log(err);
            else {
            console.log("Data written successfully\n");
            }
        });
    }
})
